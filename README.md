CONTENTS OF THIS FILE
---------------------

 * Docker compose settings for Drupal 8

INTRODUCTION
------------

Get Drupal source with composer
```
composer create-project drupal/recommended-project drupal8
```

Start docker-compose
```
docker-compose up -d
```

Drupal website
```
http://web.drupal8.docker.localhost/
```

REQUIREMENTS
-------------

- [Docker compose](https://docs.docker.com/compose/install/)
- [Composer](https://getcomposer.org/download/)

